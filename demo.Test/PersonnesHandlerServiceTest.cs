using demo.web.Models;
using demo.web.Services;
using System;
using System.Collections.Generic;
using Xunit;

namespace demo.Test
{
    public class PersonnesHandlerServiceTest
    {
        [Fact]
        public void CanGetTheListOfPersonne()
        {
        //Given 
            var service = new PersonnesHandlerService();
        //When
            IEnumerable<Personne> personnes = service.GetListPersonnes();
        //Then
            Assert.NotEmpty(personnes);
        }

        [Fact]
        public void CanGetPersonneById()
        {
        //Given
            var service = new PersonnesHandlerService();
        //When
            Personne personne = service.GetPersonneById(1);
        //Then
            Assert.NotNull(personne);
            Assert.Equal(1, personne.Id);
            Assert.Equal("John", personne.FirstName);
            Assert.Equal("Doe", personne.LastName);
        }

        [Fact]
        public void GetPersonneFromInvalidIdThrowsExecption()
        {
        //Given
            var service = new PersonnesHandlerService();
        //When
            Exception exception = Assert.Throws<Exception>(() => service.GetPersonneById(2));
        //Then
            Assert.Equal("No personne found with this id", exception.Message);
        }
    }
}
