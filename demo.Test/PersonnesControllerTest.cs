﻿using demo.web.Services;
using demo.web.Models;
using demo.web.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using Moq;
using Microsoft.AspNetCore.Mvc;

namespace demo.Test
{
    public class PersonnesControllerTest
    {
        [Fact]
        public void CanGetPersonneById()
        {
        //Given
            var mockService = new Mock<IPersonneHandlerService>();
            var expectedPersonne = new Personne(1, "John", "Doe");
            mockService.Setup(service => service.GetPersonneById(expectedPersonne.Id))
                       .Returns(expectedPersonne); //Simule la methode du service
            var controller = new PersonnesController(mockService.Object);
        //When
            var result = Assert.IsType<OkObjectResult>(controller.GetPersonneById(expectedPersonne.Id)); //Verifie qu'on a bien http 200
        //Then
            Personne personne = Assert.IsType<Personne>(result.Value);
            Assert.Equal(expectedPersonne.Id, personne.Id);
            Assert.Equal(expectedPersonne.FirstName, personne.FirstName);
            Assert.Equal(expectedPersonne.LastName, personne.LastName);
        }

        [Fact]
        public void GetPersonneFromInvalidIdGet404()
        {
        //Given
            var mockService = new Mock<IPersonneHandlerService>();
            mockService.Setup(service => service.GetPersonneById(It.IsAny<int>()))
                       .Throws(new Exception("No personne found with this id"));
            var controller = new PersonnesController(mockService.Object);
        //When
            var result = Assert.IsType<NotFoundObjectResult>(controller.GetPersonneById(1));
        //Then
            var message = Assert.IsType<string>(result.Value);
            Assert.Equal("No personne found with this id", message);
        }
    }
}
