﻿using demo.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo.web.Services
{
    public interface IPersonneHandlerService
    {
        public abstract IEnumerable<Personne> GetListPersonnes();

        public abstract Personne GetPersonneById(int id);

        public abstract Personne CreatePersonne(int id, Personne inputPersonne);
    }
}
