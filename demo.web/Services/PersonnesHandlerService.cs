﻿using demo.web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace demo.web.Services
{
    public class PersonnesHandlerService : IPersonneHandlerService
    {
        private List<Personne> Personnes;

        public PersonnesHandlerService()
        {
            Personnes = new List<Personne>();
            Personne p1 = new Personne(1, "John", "Doe");
            Personnes.Add(p1);
        }

        public Personne CreatePersonne(int id, Personne inputPersonne)
        {
            try
            {
                GetPersonneById(id);
            } catch(Exception)
            {
                if (id != inputPersonne.Id)
                    throw new Exception("Id inconsistent between url and body!");
                Personnes.Add(inputPersonne);
                return inputPersonne;
            }
            throw new Exception("This personne id already exist!");
        }

        public IEnumerable<Personne> GetListPersonnes()
        {
            return Personnes;
        }

        public Personne GetPersonneById(int id)
        {

            Personne p = Personnes.Find(personne => personne.Id == id);
            if (p == null)
                throw new Exception("No personne found with this id");
            return p;
        }

        
    }
}
