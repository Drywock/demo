﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using demo.web.Models;
using demo.web.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace demo.web.Controllers
{
    [Route("/personnes")]
    [ApiController]
    public class PersonnesController : ControllerBase
    {
        private readonly IPersonneHandlerService PersonnesService;

        public PersonnesController(IPersonneHandlerService personneHandler)
        {
            PersonnesService = personneHandler;
        }

        [HttpGet]
        public IEnumerable<Personne> GetPersonnes()
        {
            return PersonnesService.GetListPersonnes();
        }

        [HttpGet("{id}")]
        public IActionResult GetPersonneById(int id)
        {
            Personne personne;
            try
            {
                personne = PersonnesService.GetPersonneById(id);
            } catch (Exception e)
            {
                return NotFound(e.Message);
            }

            return Ok(personne);
        }

        [HttpPut("{id}")]
        public IActionResult CreatePersonne(int id, [FromBody] Personne personne)
        {
            try
            {
                Personne p = PersonnesService.CreatePersonne(id, personne);
                return Ok(p);
            } catch(Exception e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
